// header.js ()
// copyright 2019 vasanthrt
// Licensed under MIT (https://github.com/vasanthrt/mysite/blob/master/LICENSE)

window.onscroll = function() {myFunction()};

var navbar = document.getElementByClass("header");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}
