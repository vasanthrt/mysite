"""
Created By :Vasanth R
Date       :27/12/2018
Time       :14:8 PM(IST)
version    :1.0
"""


import os
import webbrowser

#======================functions()=============================

def windows():
   user=raw_input("\nenter the user's id")
   os.system("start \"\" https://api.github.com/users/"+user)

def macos():
   user=raw_input("\nenter the user's id\n")
   os.system("open \"\" https://api.github.com/users/"+user)

def linux():
   user=raw_input("\nenter the user's id\n")
   os.system("xdg-open \"\" https://api.github.com/users/"+user)

def cross_platform():
   user=raw_input("\nenter the user's id\n")
   webbrowser.open("https://api.github.com/users/"+user)

#======================program===============================

print"======================"
print"GitHub User INFO (API)"
print"======================\n\n"
print"choose your os\n======MENU======\n"
print"1)Windows"
print"2)MacOS"
print"3)Linux"
print"4)Cross Platform"
ch = input("enter your choice:")
if ch == 1:
    windows()
elif ch == 2:
    macos()
elif ch == 3:
    linux()
elif ch == 4:
    cross_platform()
else:
    print"invalid choice!\nprogram exited normally"
